import scala.annotation.tailrec

/**
 * Created by senaga on 05/09/15.
 */
object Palindrome extends App {

  def isPalindrome(text: String): Boolean = {
    @tailrec
    def recursiveCheck(text: String, size: Int): Boolean = {
      text match {
        case _ if size <= 1 => true
        case _ if (text(0) != text(size - 1)) => false
        case _ => recursiveCheck(text.substring(1), size - 2)
      }
    }
    val compress_text = text.trim()
    compress_text.isEmpty match {
      case true => false
      case _ => recursiveCheck(compress_text, compress_text.length)
    }
  }

  if (args.length != 1) {
    println("Uso: ./script.sh arg\n")
    println("Obs: O programa eh case sensitive: considera Ana e ana palavras diferentes (somente a ultima eh palindrome")
  } else {
    println(isPalindrome(args(0)))
  }
}
