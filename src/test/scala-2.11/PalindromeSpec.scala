import org.scalatest.{ShouldMatchers, FlatSpec}

import scala.util.Random

class PalindromeSpec extends FlatSpec with ShouldMatchers {

  "isPalindrome" should "return false given an emtpy string" in {
    Palindrome.isPalindrome("") should equal(false)
  }

  "isPalindrome" should "return false given an string with one space" in {
    Palindrome.isPalindrome(" ") should equal(false)
  }

  "isPalindrome" should "return false given an string with two or more space" in {
    def createSpaceString(): String = {
      (2 to (2+Random.nextInt(10)) map (_ => " ")).toList mkString
    }
    Palindrome.isPalindrome(createSpaceString()) should equal(false)
  }

  "isPalindrome" should "return true given an a" in {
    Palindrome.isPalindrome("a") should equal(true)
  }

  "isPalindrome" should "return true given an aa" in {
    Palindrome.isPalindrome("aa") should equal(true)
  }

  "isPalindrome" should "return false given an Ana" in {
    Palindrome.isPalindrome("Ana") should equal(false)
  }

  "isPalindrome" should "return true given an ana" in {
    Palindrome.isPalindrome("ana") should equal(true)
  }

  "isPalindrome" should "return false given an bc" in {
    Palindrome.isPalindrome("bc") should equal(false)
  }

  "isPalindrome" should "return true given an ovo" in {
    Palindrome.isPalindrome("ovo") should equal(true)
  }

  "isPalindrome" should "return true given an SATOR AREPO TENET OPERA ROTAS" in {
    Palindrome.isPalindrome("SATOR AREPO TENET OPERA ROTAS") should equal(true)
  }
}
